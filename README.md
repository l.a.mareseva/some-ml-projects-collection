# Some ML projects collection

## Table of Contents
1. [SGD and Adam`s optimization](#SGD_Adam)
2. [logistic regression](#log_reg)
3. [Classification with sklearn trees](#trees_classification)
4. [Text processing](#text_processing)
5. [CNN for binary classification](#CNN_classification)
6. [Topic Modeling with LDA](#lda_topic)
7. [Time-series prediction](#time_prediction)
8. [Images noise remover](#noise_remove)


## 1  Stochastic gradient descent (SGD) and Adam`s optimization algorithms <a name="SGD_Adam"></a>

Stochastic gradient descent (SGD) and Adam`s optimization algorithms were
[implemented](https://colab.research.google.com/drive/1ivwiEFUT0GASX4yhtYLUJaUhWKl_TK39?usp=sharing#scrollTo=B2Cx5p9k76Jq) for a function of two variables
```
f = 2.48x2 + 2.48y2 + 2
```
## 2 logistic regression <a name="log_reg"></a>

Logistic regression method was [applyed](https://colab.research.google.com/drive/1Norm3cy97agD4ksBw_CSg2jB2WlTZ1Ln#scrollTo=1ufmdTonvzZB) to predict the biological response of a molecule. Optimize() function was modified to implement the stochastic gradient descent (SGD) method and adam.

Observed plots are shown in the colab notebook and are the result using models
with different values of the learning rate.


## 3 Classification with sklearn trees <a name="trees_classification"></a>

Small decision tree with max depth 3, deep decision tree, random forest on small trees with max depth 3, random forest on deep trees classifiers were [implemented](https://drive.google.com/file/d/1d-ls_IyZjJC3u8EDhCfUcEp-cy4JNMpW/view) using sklearn library to predict "Activity" (biological response of the molecule) field from the "bioresponse.csv" dataset.

Classification_report was used to get info about precision,
recall, accuracy, F1-score, and log_loss function.
To plot models curves sklearn.metrics plot_precision_recall_curve and
plot_roc_curve were used, or plots based on precision_recall_curve and roc_curve
data.

New classifier, which avoids Type II (False Negative) errors was built. In wrapper
for RandomForestClassifier GridSearchCV used to fit according to the combinations of
parameters and give the best model to full training data for downstream use.

 Then this
model is used as a base for a classifier, that will have zero False Negative errors. False
Negative error equal to 0 was archived, though it led to a decrease in all negative values.
Below is comparration of metrics for the new model, RandomForestClassifier and
RandomForestClassifier with best parameters.

![alt text](https://sun9-85.userapi.com/impg/IwStKReqnPFPOS4pEcsyVR5_2fCdhYWegofulQ/f5mmnQBEh9Y.jpg?size=782x639&quality=96&sign=4b06cfdce3ee094bfc7c59926ceff141&type=album)
![alt text](https://sun9-42.userapi.com/impg/x9D-mWyAQMXHbSW3Xk6goDFCekjj1ABPgcZRZg/nYnytJX6iYI.jpg?size=870x715&quality=96&sign=5ba9f3a5b3df734e45ae2b6bc4be684a&type=album)

## 4 Text processing <a name="text_processing"></a>

[Trying data processing](https://drive.google.com/file/d/18UNEYtJrkC7RVh6AfGeONLa0EK_rwhOv/view) on Alice in Wonderland text (cutting off introduction, splitting text into
chapters, converting to lower case, removing irrelevant characters, removing stop words,
lemmatization and stemming).

Goal is to find top 10 most important in terms of TF-IDF metric words from each chapter.
Result is shown below.

![alt text](https://media.discordapp.net/attachments/389543693561888778/899623769453252658/Screenshot_from_2021-10-18_14-40-56.png?width=590&height=624)

Since understanding the meaning of some tokens was hard, in addition, I have
calculated tf-idf for texts without stemming, but original tokens were used as base. Based on this info I decided than we can summurize the results and give chapters folowwing names:
| # | name |#| name |#| name |
| --|------|-|------|-|------|
| 1.| Alice opens door and fall| 5.| Young pigeon and his egg| 9.| Turtle mocks|
| 2.| Mouse, Alice and cat| 6.| Cat baby mad at pig | 10.| Turtle still mocks gryphon because of beauti soup|
| 3.| Mouse and dodo knows prize| 7.| Dormous the hatter| 11.| King and dormous the hatter on a court|
| 4.| Rabbit in window | 8.| Queen and king in garden| 12.| King juri dream|

Top 10 most used verbs in sentences with Alice: ​
```
'say', 'brave', 'll',
'think', 'wouldn', 'say', 'think—', 'see', 'wonder',
'latitude'
```

## 5 CNN for binary classification <a name="CNN_classification"></a>

CNN with 3 convolutional layers for binary classification was built and fit. In [notebook](https://colab.research.google.com/drive/1PVnIkRKjthqToid9DbKqy7SniZVw7Uhg) results for CNN with different parameters can be obtained.

Also transfer Learning model was built and trained using pre-trained VGG16-model
weights from keras application.

## 6 Topic Modeling with LDA <a name="lda_topic"></a>

Topic Modeling with LDA [was performed](https://colab.research.google.com/drive/1T0pI-uPcHsbY6Vd2IKaks38bQ7txm9cR#scrollTo=bPAjAmttm3_6) based on data of the abstracts from the papers of authors associated with ITMO University.
![tex](https://media.discordapp.net/attachments/389543693561888778/899639639474855946/Screenshot_from_2021-10-18_15-46-23.png)

The above plots show that coherence score increases with the number of topics, with
a decline after 5 nums of topics, it may make better sense to pick the model that gave the
highest CV before flattening out or a major drop. Thus topic number around 5 have a
good coherence score. But predictive score is lower for num topics > 5. Since predictive
likelihood and human judgment are often not correlated, num of topics was chosen
according to coherence.

Based on result for LDA model with 5 topics i think, that sutable names are:
| # | name |#| name |#| name |
| --|------|-|------|-|------|
| 1.| Optics and lasers| 4.| Biological studies on different populations|
| 2.| Laser usage for different materials| 5.| Visualization for model through pyLDAvis package.|
| 3.| Information systems and networks|   |  |

Some visualisations
![img](https://media.discordapp.net/attachments/389543693561888778/899640694468116510/Screenshot_from_2021-10-18_15-50-35.png?width=1079&height=624)


## 7 Time-series prediction <a name="time_prediction"></a>

Multi-step LSTM prediction model and ARIMA prediction model were
implemented for time-series:

* Jena Climate dataset recorded by the Max Planck Institute for
Biogeochemistry

* Airplane Crashes and Fatalities Since 1908


Colab Notebook:

[ARIMA_plane](https://drive.google.com/file/d/1JUy2Zg_MA8JRqMFtUsPsVgra4UJw_hjG/view?usp=sharing)

[ARIMA_weather](https://drive.google.com/file/d/17PE0a33ImOO5x--UyVJD1G3bG5qqWlEY/view?usp=sharing)

[LSTM](https://drive.google.com/file/d/1cp4u-SgIP94ZL-gPMpJZiEZPf5F4D6lB/view?usp=sharing)


Based on MAPE, ARIMA is a better model for both time series

## 8 Images noise remover <a name="noise_remove"></a>

Convolutional denoising autoencoder [was developed](https://drive.google.com/file/d/1jAM5A0i4smeoQWUzxlbuwL8C9TrWNqQ7/view?usp=sharing), which removes noise from
the images from CIFAR10 dataset.
Onto train and test data was casted noise as:
```
x_train + noise_factor*np.random.normal(loc=​ 0.0​ ,scale=​ 1.0​ ,size=x_train.shape)
```

With autoencoder as:
```
autoencoder.fit(x_train_noisy, x_train,
    epochs=​ 5 ​ ,
    batch_size=​ 128​ ,
    shuffle=​ True​ ,
    validation_data=(x_test_noisy, x_test))
```
results were obtained:
![img](https://media.discordapp.net/attachments/389543693561888778/899642846448083004/Screenshot_from_2021-10-18_15-59-14.png?width=1076&height=217)



